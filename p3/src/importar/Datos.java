package importar;

import java.util.ArrayList;

public class Datos {

	private String aerolinea;
	private int numeroVuelo;
	private String origen;
	private String destino;
	private String horaSalida;
	private String horaLlegada;
	private String avion;
	private int numSillas;
	private boolean[] dias;
	private String tipoVuelo;
	private int numSillasMax;
	private double valorMinuto;
	
	private ArrayList<Datos> datos;
	private ArrayList<Datos> datosMinutoAerolinea;
	
	public Datos ( ){
		datos = new ArrayList<Datos>();
		datosMinutoAerolinea = new ArrayList<Datos>();
	}
	
	public Datos (String aerolinea, int numeroVuelo, String origen, String destino, String horaSalida, String horaLlegada, String avion, int numSillas, boolean[] dias, String tipoVuelo){
		this.aerolinea = aerolinea;
		this.numeroVuelo = numeroVuelo;
		this.origen = origen;
		this.destino = destino;
		this.horaSalida = horaSalida;
		this.horaLlegada = horaLlegada;
		this.avion = avion;
		this.numSillas = numSillas;
		this.dias = dias;
		this.tipoVuelo = tipoVuelo;
	}
	
	public Datos (String aerolinea, double valorMinuto, int numSillasMax){
		this.aerolinea = aerolinea;
		this.valorMinuto = valorMinuto;
		this.numSillasMax = numSillasMax;
	}
	
	public String darAerolinea ( ){
		return aerolinea;
	}
	
	public int darNumeroVuelo ( ){
		return numeroVuelo;
	}
	
	public String darOrigen ( ){
		return origen;
	}
	
	public String darDestino ( ){
		return destino;
	}
	
	public String darHoraSalida ( ){
		return horaSalida;
	}
	
	public String darHoraLlegada ( ){
		return horaLlegada;
	}
	
	public String darTipoAvion ( ){
		return avion;
	}
	
	public int darNumeroSillas ( ){
		return numSillas;
	}
	
	public boolean[] darDiasVuelo ( ){
		return dias;
	}
	
	public String darTipoVuelo ( ){
		return tipoVuelo;
	}
	
	public double darValorMinuto ( ){
		return valorMinuto;
	}
	
	public int darNumeroSillasMax ( ){
		return numSillasMax;
	}
	
	public ArrayList<Datos> darDatos ( ){
		return datos;
	}
	
	public ArrayList<Datos> darDatosMinutoAerolinea ( ){
		return datosMinutoAerolinea;
	}
	
	public void agregar(Datos dato){
		datos.add(dato);
	}
	
	public void agregarDatosMinutoAerolinea(Datos dato){
		datosMinutoAerolinea.add(dato);
	}
	
	public void imprimirVuelos ( ){
		for (int i = 0; i < datos.size(); i++){
			Datos actual = datos.get(i);
			boolean[] dias = actual.darDiasVuelo();
			String vuelo = "";
			for (int j = 0; j < dias.length; j++){
				if (dias[j]){
					vuelo = vuelo + "x ";
				} else {
					vuelo = vuelo + "  ";
				}
			}
			
			System.out.println(actual.darAerolinea() + " " + actual.darNumeroVuelo() + " " + actual.darOrigen() + " " + actual.darDestino() + " " + actual.darHoraSalida() + " " + actual.darHoraLlegada() + " " + actual.darTipoAvion() + " " + actual.darNumeroSillas() + " " + 
			vuelo + actual.darTipoVuelo());
		}
	}
	
	public void imprimirMinutoAerolinea ( ){
		for (int i = 0; i < datosMinutoAerolinea.size(); i++){
			Datos actual = datosMinutoAerolinea.get(i);
			System.out.println(actual.darAerolinea() + " $" + actual.darValorMinuto() + " USD " + actual.darNumeroSillasMax());
		}
	}
	
	
}
