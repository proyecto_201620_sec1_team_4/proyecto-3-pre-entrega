package importar;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ImportarDatos {
	
	private final static String RUTA = "./data/ItinerarioAeroCivil-v2.xlsx";
	private Datos dat;
	private File archivo;
	private FileInputStream input;
	private XSSFWorkbook libro;

	public ImportarDatos ( ){
		dat = new Datos();
		
		try {
			archivo = new File(RUTA);
			input = new FileInputStream(archivo);
			libro = new XSSFWorkbook(input);
			leerVuelos();
			leerMinutoAerolinea();
			input.close();
		} catch (Exception e){
			e.getMessage();
		}
		
	}
	
	@SuppressWarnings({ "deprecation" })
	public void leerVuelos ( ) throws IOException{
		
		XSSFSheet hoja = libro.getSheetAt(0);
		
		int numFilas = hoja.getPhysicalNumberOfRows();
		int numColumnas = hoja.getRow(numFilas-1).getPhysicalNumberOfCells();

		SimpleDateFormat dateFormat = new SimpleDateFormat("H:mm");
		Datos temp = null;
		for (int i = 2; i < numFilas; i++){

			String aerolinea = ""; int numeroVuelo = -1; String origen = "";
			String destino = ""; String horaLlegada = null; String horaSalida = null;
			String avion = ""; int numSillas = -1; boolean[] dias = new boolean[7]; String tipoVuelo = "";
			
			for (int j = 0; j < numColumnas; j++){
				Cell celda = hoja.getRow(i).getCell(j);
				
				if (Cell.CELL_TYPE_STRING == celda.getCellType()){
					if (celda.getColumnIndex() == 0){
						aerolinea = celda.getStringCellValue();
					} else if (celda.getColumnIndex() == 2){
						origen = celda.getStringCellValue();
					} else if (celda.getColumnIndex() == 3){
						destino = celda.getStringCellValue();
					} else if (celda.getColumnIndex() == 6){
						avion = celda.getStringCellValue();
					} else if (celda.getColumnIndex() > 7 && celda.getColumnIndex() < numColumnas-1){
						dias[j - 8] = true; 
					} else {
						tipoVuelo = celda.getStringCellValue();
					}
				} else if (Cell.CELL_TYPE_NUMERIC == celda.getCellType()){
					if (DateUtil.isCellDateFormatted(celda)){
						if (celda.getColumnIndex() == 4){
							horaSalida = dateFormat.format(celda.getDateCellValue());
						} else {
							horaLlegada = dateFormat.format(celda.getDateCellValue());
						}
					} else {
						if (celda.getColumnIndex() == 1){
							numeroVuelo = (int) celda.getNumericCellValue();
						} else {
							numSillas = (int) celda.getNumericCellValue();
						}
					}
				} else if (Cell.CELL_TYPE_BLANK == celda.getCellType()){
					if (celda.getColumnIndex() == 0){
						aerolinea = temp.darAerolinea();
					} else if (celda.getColumnIndex() == 1){
						numeroVuelo = temp.darNumeroVuelo();
					} else if (celda.getColumnIndex() == 2){
						origen = temp.darOrigen();
					} else if (celda.getColumnIndex() == 3){
						destino = temp.darDestino();
					} else if (celda.getColumnIndex() == 4){
						horaSalida = temp.darHoraSalida();
					} else if (celda.getColumnIndex() == 5){
						horaLlegada = temp.darHoraLlegada();
					} else if (celda.getColumnIndex() > 7 && celda.getColumnIndex() < numColumnas){
						dias[j - 8] = false;
					}
				}
			}
			Datos nuevo = new Datos(aerolinea, numeroVuelo, origen, destino, horaSalida, horaLlegada, avion, numSillas, dias, tipoVuelo);
			temp = nuevo;
			dat.agregar(nuevo);
		}
		dat.imprimirVuelos();
		
	}
	
	@SuppressWarnings("deprecation")
	public void leerMinutoAerolinea ( ) throws IOException{
		XSSFSheet hoja = libro.getSheetAt(1);
		
		Iterator<Row> iteradorFilas = hoja.iterator();
		Row fila = iteradorFilas.next();
		
		while (iteradorFilas.hasNext()){
			
			fila = iteradorFilas.next();
			
			//Iterar sobre las columnas
			Iterator<Cell> iteradorColumna = fila.cellIterator();
			Cell celda = null;
			String aerolinea = ""; double valorMinuto = -1.0; int numSillasMax = -1;
			
			while(iteradorColumna.hasNext()){
				
				celda = iteradorColumna.next();
				
				switch (celda.getCellType()){
					case Cell.CELL_TYPE_NUMERIC:
						numSillasMax = (int) celda.getNumericCellValue();
						break;
					case Cell.CELL_TYPE_STRING:
						if (celda.getColumnIndex() == 0){
							aerolinea = celda.getStringCellValue();
						} else {
							String[] tmp = celda.getStringCellValue().split(" ");
							valorMinuto = Double.parseDouble(tmp[0]);
						}
						break;
				}
			}
			Datos nuevos = new Datos(aerolinea, valorMinuto, numSillasMax);
			dat.agregarDatosMinutoAerolinea(nuevos);
		}
		dat.imprimirMinutoAerolinea();
	}
	
	public static void main(String[] args){
		new ImportarDatos();
	}
}
