package estructuras;

import java.util.Iterator;

public class Pila <T>
{
	
	//-------------------------------------------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------------------------------------------
	/**
	 * lista que se va a implementar para hacer una pila
	 */
	private ListaEncadenada<T> lista;
	
	//-------------------------------------------------------------------------------------------------
	//Constructor
	//-------------------------------------------------------------------------------------------------
	/**
	 * Crea una pila
	 */
	public Pila() 
	{
		lista = new ListaEncadenada<T>();
	}
	
	//-------------------------------------------------------------------------------------------------
	//M�todos
	//-------------------------------------------------------------------------------------------------
	/**
	 * @return True si la pila est� vac�a, falso de lo contrario.
	 */
	public boolean isEmpty()
	{
		return lista.isEmpty();
	}
	
	/**
	 * @return el tama�o de la pila
	 */
	public int size() 
	{
		return lista.size();
	}
	
	/**
	 * Agrega el nuevo objeto de primero en la pila
	 * @param item Objeto a agregar
	 */
	public void push(T item)
	{
		lista.anadirPrimero(item);
	}

	/**
	 * Quita el primer elemento que hay en la pila
	 * @return Elemento que est� primero en la pila
	 */
	public T pop() 
	{
		return lista.quitarPrimero();
	}

	/**
	 * @return El primer elemento de la pila
	 */
	public T top()
	{
		return lista.darPrimero();
	}
	
    /**
     * Returns an iterator to this stack that iterates through the items in LIFO order.
     *
     * @return an iterator to this stack that iterates through the items in LIFO order
     */
    public Iterator<T> iterator() {
        return lista.iterator();
    }

}
