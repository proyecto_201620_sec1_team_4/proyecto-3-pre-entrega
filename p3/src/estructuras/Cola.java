package estructuras;

import java.util.Iterator;

public class Cola <T> {

	//-------------------------------------------------------------------------------------------------
	//Atributos
	//-------------------------------------------------------------------------------------------------
	/**
	 * lista que se va a usar para implementar una cola
	 */
	private ListaEncadenada<T> lista;
	
	//-------------------------------------------------------------------------------------------------
	//Constructor
	//-------------------------------------------------------------------------------------------------
	/**
	 * Crea una cola
	 */
	public Cola() 
	{
		lista = new ListaEncadenada<T>();
	}

	//-------------------------------------------------------------------------------------------------
	//M�todos
	//-------------------------------------------------------------------------------------------------
	/**
	 * Dice si la cola est� vac�o o no
	 * @return True si la cola est� vac�a, falso de lo contrario
	 */
	public boolean isEmpty() {
		return lista.isEmpty();
	}
	
	/**
	 * Indica el tama�o de la cola
	 * @return tama�o de la cola
	 */
	public int size() {
		return lista.size();
	}

	/**
	 * A�ade el nuevo objeto al final de la cola
	 * @param item Objeto a agregar
	 */
	public void enqueue(T item) {
		lista.anadirUltimo(item);
	}

	/**
	 * Quita el primer objeto de la cola
	 * @return El primer objeto
	 */
	public T dequeue() {
		return lista.quitarPrimero();
	}

	/**
	 * @return da la información del objeto que está primero en la cola
	 */
	public T top() {
		return lista.darPrimero();
	}
	
	public Iterator<T> iterator(){
		return lista.iterator();
	}

}
